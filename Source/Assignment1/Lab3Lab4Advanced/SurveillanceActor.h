// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SurveillanceActor.generated.h"

UCLASS()
class ASSIGNMENT1_API ASurveillanceActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASurveillanceActor();

	UPROPERTY(EditAnywhere)
		float MaxDistance;

	UPROPERTY(EditAnywhere);
	UStaticMeshComponent *mVisibleComponent;

	UPROPERTY(EditAnywhere);
	AActor *SurveillanceTarget;

	UPROPERTY(EditAnywhere);
	UMaterial *OffMaterial;

	UPROPERTY(EditAnywhere);
	UMaterial *OnMaterial;

	// Targets that are activated by this actor.
	UPROPERTY(EditAnywhere)
		TArray<AActor *> Activatable;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
