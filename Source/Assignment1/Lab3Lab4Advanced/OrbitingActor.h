// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "OrbitingActor.generated.h"

UCLASS()
class ASSIGNMENT1_API AOrbitingActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOrbitingActor();

	UPROPERTY(EditAnywhere)
		float RotationSpeed;

	UPROPERTY(EditAnywhere)
		AActor *OrbitActor;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *mVisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		FVector RotationRadius;

	UPROPERTY(VisibleAnywhere)
		float RotationDegrees;

	UPROPERTY(VisibleAnywhere)
		FVector OrbitLocation;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
