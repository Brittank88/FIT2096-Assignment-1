// Fill out your copyright notice in the Description page of Project Settings.


#include "LavaFloorActor.h"

// Sets default values
ALavaFloorActor::ALavaFloorActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup root and visible components.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);
	mVisibleComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	mVisibleComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// Fix performance hit caused by dynamic shadows.
	mVisibleComponent->bCastDynamicShadow = false;

	// Find and set static mesh.
	ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Game/StarterContent/Shapes/Shape_Plane.Shape_Plane"));
	if (Mesh.Succeeded()) mVisibleComponent->SetStaticMesh(Mesh.Object);

	// Find and set material.
	ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("/Game/AssignmentFiles/Lab3Lab4Advanced/Materials/M_Lava.M_Lava"));
	if (Mat.Succeeded()) mVisibleComponent->SetMaterial(0, Mat.Object);
}

// Called when the game starts or when spawned
void ALavaFloorActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALavaFloorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// For every killable actor, check if it has fallen into the lava.
	for (AActor *KillTarget : Killable) {
		// Use vector components to check if actor has fallen into lava.
		FVector Origin, BoxExtent;
		GetActorBounds(false, Origin, BoxExtent);
		if (
			KillTarget != nullptr &&
			KillTarget->GetActorLocation().Z <= GetActorLocation().Z &&
			KillTarget->GetActorLocation().X <= (GetActorLocation() + BoxExtent).X &&
			KillTarget->GetActorLocation().X >= (GetActorLocation() - BoxExtent).X &&
			KillTarget->GetActorLocation().Y <= (GetActorLocation() + BoxExtent).Y &&
			KillTarget->GetActorLocation().Y >= (GetActorLocation() - BoxExtent).Y
		) UKismetSystemLibrary::QuitGame(GetWorld(), GetWorld()->GetFirstPlayerController(), EQuitPreference::Quit, false);
	}
}

