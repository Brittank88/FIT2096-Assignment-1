// Fill out your copyright notice in the Description page of Project Settings.


#include "ChasePlayerActor.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
AChasePlayerActor::AChasePlayerActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);
	mVisibleComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	mVisibleComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SetActorScale3D(FVector(0.5, 0.5, 0.5));
	mVisibleComponent->AddLocalRotation(FRotator(-90, 0, 0));

	// Find and set static mesh.
	ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Game/StarterContent/Shapes/Shape_Cone.Shape_Cone"));
	if (Mesh.Succeeded()) mVisibleComponent->SetStaticMesh(Mesh.Object);

	// Find and set material.
	ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("/Game/AssignmentFiles/Lab3Lab4Advanced/Materials/M_Lava.M_Lava"));
	if (Mat.Succeeded()) mVisibleComponent->SetMaterial(0, Mat.Object);

	Speed = 200.f;
	bIsChasing = false;
}

// Called when the game starts or when spawned
void AChasePlayerActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AChasePlayerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (FollowTarget != nullptr && bIsChasing) {
		FVector TargetDirection = FollowTarget->GetActorLocation() - GetActorLocation();
		FVector Origin, BoxExtent;
		GetActorBounds(false, Origin, BoxExtent);
		if (TargetDirection.Size() <= BoxExtent.X * 2)
			UKismetSystemLibrary::QuitGame(GetWorld(), GetWorld()->GetFirstPlayerController(), EQuitPreference::Quit, false);
		TargetDirection.Normalize();
		SetActorLocation(GetActorLocation() + (TargetDirection * Speed * DeltaTime));

		SetActorRotation(TargetDirection.ToOrientationQuat());
	}
}

