// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SquarePathActor.generated.h"

UCLASS()
class ASSIGNMENT1_API ASquarePathActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASquarePathActor();

	// Visible component for the actor.
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent *mVisibleComponent;

	// Distance moved for each side length of the square path.
	UPROPERTY(EditAnywhere)
	float SquarePathSideLength;

	// Speed of movement.
	UPROPERTY(EditAnywhere)
		float Speed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Direction vector for movement.
	UPROPERTY(VisibleAnywhere)
		FVector Direction;

	// Initial position of platform when movement cycle began.
	UPROPERTY(VisibleAnywhere)
		FVector InitialPosition;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
