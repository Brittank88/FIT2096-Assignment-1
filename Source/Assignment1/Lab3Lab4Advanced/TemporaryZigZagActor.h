// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TemporaryZigZagActor.generated.h"

UCLASS()
class ASSIGNMENT1_API ATemporaryZigZagActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATemporaryZigZagActor();

	// Visible component for the actor.
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *mVisibleComponent;

	// How far we travel.
	UPROPERTY(EditAnywhere)
		float TravelDistance;

	// How fast we travel!
	UPROPERTY(EditAnywhere)
		float Speed;

	// How close we travel to our destination or origin before reversing our direction.
	UPROPERTY(EditAnywhere)
		float Tolerance;

	// Begins movement which lasts for 5 seconds by default.
	virtual void BeginMovement(float Duration = 5.f);

	// TimerHandle for movement stop.
	FTimerHandle MovementStopTimer;

	// Deactivated and activated state textures.
	UPROPERTY(EditAnywhere);
	UMaterial *OffMaterial;
	UPROPERTY(EditAnywhere);
	UMaterial *OnMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Our initial position.
	FVector StartingPosition;

	// Our direction of travel.
	float Direction;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
