// Fill out your copyright notice in the Description page of Project Settings.

#include "SurveillanceActor.h"
#include "TemporaryZigZagActor.h"
#include "ChasePlayerActor.h"

// Sets default values
ASurveillanceActor::ASurveillanceActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));
	if (Mesh.Succeeded()) mVisibleComponent->SetStaticMesh(Mesh.Object);

	// Find and set materials.
	ConstructorHelpers::FObjectFinder<UMaterial> OffMaterialObj(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile.M_Tech_Hex_Tile"));
	ConstructorHelpers::FObjectFinder<UMaterial> OnMaterialObj(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile_Pulse.M_Tech_Hex_Tile_Pulse"));
	if (OffMaterialObj.Succeeded()) {
		OffMaterial = OffMaterialObj.Object;
		// Begin disabled.
		mVisibleComponent->SetMaterial(0, OffMaterial);
	}
	if (OnMaterialObj.Succeeded()) OnMaterial = OnMaterialObj.Object;

	MaxDistance = 500.f;
}

// Called when the game starts or when spawned
void ASurveillanceActor::BeginPlay()
{
	Super::BeginPlay();

	mVisibleComponent->SetMaterial(0, OffMaterial);
}

// Called every frame
void ASurveillanceActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SurveillanceTarget != nullptr) {
		FVector SurveillanceDirection = SurveillanceTarget->GetActorLocation() - GetActorLocation();

		if (SurveillanceDirection.Size() < MaxDistance) {
			SurveillanceDirection.Normalize();
			float DotProduct = FVector::DotProduct(GetActorForwardVector(), SurveillanceDirection);

			if (DotProduct > 0) {
				mVisibleComponent->SetMaterial(0, OnMaterial);

				for (AActor *ActivateTarget : Activatable) {
					if (ActivateTarget != nullptr) {
						// Attempt to cast to our TemporaryZigZagActor.
						ATemporaryZigZagActor *TemporaryZigZagActorCast = Cast<ATemporaryZigZagActor>(ActivateTarget);
						if (TemporaryZigZagActorCast) TemporaryZigZagActorCast->BeginMovement();

						// Attempt to cast to our ChasePlayerActor.
						AChasePlayerActor *ChasePlayerActorCast = Cast<AChasePlayerActor>(ActivateTarget);
						if (ChasePlayerActorCast) ChasePlayerActorCast->bIsChasing = true;
					}
				}
			}
			else mVisibleComponent->SetMaterial(0, OffMaterial);
		// Should fix bug where walking out of range whilst this actor is activated keeps it activated.
		} else mVisibleComponent->SetMaterial(0, OffMaterial);
	}
}

