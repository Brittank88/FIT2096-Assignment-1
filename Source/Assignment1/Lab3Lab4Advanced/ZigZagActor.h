// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ZigZagActor.generated.h"

UCLASS()
class ASSIGNMENT1_API AZigZagActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AZigZagActor();

	// Visible component for the actor.
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *mVisibleComponent;

	// How far we travel.
	UPROPERTY(EditAnywhere)
		float TravelDistance;

	// How fast we travel!
	UPROPERTY(EditAnywhere)
		float Speed;

	// How close we travel to our destination or origin before reversing our direction.
	UPROPERTY(EditAnywhere)
		float Tolerance;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Our initial position.
	FVector StartingPosition;

	// Our direction of travel.
	float Direction;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
