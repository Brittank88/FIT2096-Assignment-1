// Fill out your copyright notice in the Description page of Project Settings.


#include "OrbitingActor.h"

// Sets default values
AOrbitingActor::AOrbitingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);

	// Find and set static mesh.
	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObj(TEXT("/Game/StarterContent/Shapes/Shape_WideCapsule.Shape_WideCapsule"));
	if (CubeMeshObj.Succeeded()) mVisibleComponent->SetStaticMesh(CubeMeshObj.Object);

	// Find and set material.
	ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("/Game/StarterContent/Materials/M_Concrete_Tiles.M_Concrete_Tiles"));
	if (Mat.Succeeded()) mVisibleComponent->SetMaterial(0, Mat.Object);

	RotationSpeed = 100.f;
	RotationDegrees = 0;
}

// Called when the game starts or when spawned
void AOrbitingActor::BeginPlay()
{
	Super::BeginPlay();

	if (OrbitActor != nullptr) OrbitLocation = OrbitActor->GetActorLocation();
	else OrbitLocation = FVector(GetWorld()->OriginLocation);

	OrbitLocation.SetComponentForAxis(EAxis::Z, GetActorLocation().Z);

	RotationRadius = GetActorLocation() - OrbitLocation;
}

// Called every frame
void AOrbitingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (OrbitActor != nullptr) OrbitLocation = OrbitActor->GetActorLocation();

	RotationDegrees += RotationSpeed * DeltaTime;
	if (RotationDegrees > 360) RotationDegrees -= 360;

	SetActorLocation(OrbitLocation + RotationRadius.RotateAngleAxis(RotationDegrees, FVector(0, 0, 1)));

	FVector ToOrigin = FVector(GetWorld()->OriginLocation) - GetActorLocation();
	ToOrigin.Normalize();
	SetActorRotation(
		FQuat(
			FRotator(
				0,
				FMath::RadiansToDegrees(
					FMath::Atan2(
						ToOrigin.X * FVector::ForwardVector.Y + ToOrigin.Y * FVector::ForwardVector.X,
						FVector::DotProduct(ToOrigin, FVector::ForwardVector)
					)
				),
				0
			)
		)
	);
}

