// Fill out your copyright notice in the Description page of Project Settings.


#include "TemporaryZigZagActor.h"

// Sets default values
ATemporaryZigZagActor::ATemporaryZigZagActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup root and visible components.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);

	// Find and set static mesh.
	ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	if (Mesh.Succeeded()) mVisibleComponent->SetStaticMesh(Mesh.Object);

	// Find and set materials.
	ConstructorHelpers::FObjectFinder<UMaterial> OffMaterialObj(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile.M_Tech_Hex_Tile"));
	ConstructorHelpers::FObjectFinder<UMaterial> OnMaterialObj(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile_Pulse.M_Tech_Hex_Tile_Pulse"));
	if (OffMaterialObj.Succeeded()) {
		OffMaterial = OffMaterialObj.Object;
		// Begin disabled.
		mVisibleComponent->SetMaterial(0, OffMaterial);
	}
	if (OnMaterialObj.Succeeded()) OnMaterial = OnMaterialObj.Object;

	// Setup for zigzag movement.
	TravelDistance = 200.f;
	Speed = 0;	// Begin disabled.
	Tolerance = 10.f;
	Direction = 1.f;
}

// Called when the game starts or when spawned
void ATemporaryZigZagActor::BeginPlay()
{
	Super::BeginPlay();
	
	// Note our initial position so we can calculate our target from it.
	StartingPosition = GetActorLocation();
}

// Called every frame
void ATemporaryZigZagActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Skip all this calculation if we don't even need to do it anyways.
	if (Speed > 0) {
		// Get our direction as a vector.
		FVector DirectionVector = GetActorForwardVector() * Direction;
		// Get our current position prior to setting actor location so we can do some checks.
		FVector CurrentPosition = GetActorLocation();
		// Calculate our target position (our destination).
		FVector TargetPosition = StartingPosition + (DirectionVector * TravelDistance);
		// Increment our current position variable.
		CurrentPosition += DirectionVector * Speed * DeltaTime;
		// If we're within the tolerance range of our destination or origin, flip the direction of travel.
		UE_LOG(LogTemp, Warning, TEXT("Current: %s | Target: %s."), *CurrentPosition.ToString(), *TargetPosition.ToString());
		if (FVector::Dist(CurrentPosition, TargetPosition) <= Tolerance)
		{
			CurrentPosition = TargetPosition;
			StartingPosition = CurrentPosition;
			Direction *= -1;
		}
		// Set our location.
		SetActorLocation(CurrentPosition);
	}
}

void ATemporaryZigZagActor::BeginMovement(float Duration)
{
	// Allow the actor to move.
	Speed = 200.f;
	mVisibleComponent->SetMaterial(0, OnMaterial);

	// In 5 seconds, disallow actor movement once more.
	GetWorldTimerManager().SetTimer(MovementStopTimer, [this]() {
		Speed = 0;
		mVisibleComponent->SetMaterial(0, OffMaterial);
	}, Duration, false);
}