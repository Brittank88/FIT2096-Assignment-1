// Fill out your copyright notice in the Description page of Project Settings.


#include "ZigZagActor.h"

// Sets default values
AZigZagActor::AZigZagActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup root and visible components.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);

	// Find and set static mesh.
	ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	if (Mesh.Succeeded()) mVisibleComponent->SetStaticMesh(Mesh.Object);

	// Find and set material.
	ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("/Game/StarterContent/Materials/M_Concrete_Tiles.M_Concrete_Tiles"));
	if (Mat.Succeeded()) mVisibleComponent->SetMaterial(0, Mat.Object);

	// Setup for zigzag movement.
	TravelDistance = 200.f;
	Speed = 200.f;
	Tolerance = 10.f;
	Direction = 1.f;
}

// Called when the game starts or when spawned
void AZigZagActor::BeginPlay()
{
	Super::BeginPlay();

	// Note our initial position so we can calculate our target from it.
	StartingPosition = GetActorLocation();
}

// Called every frame
void AZigZagActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Get our direction as a vector.
	FVector DirectionVector = GetActorForwardVector() * Direction;
	// Get our current position prior to setting actor location so we can do some checks.
	FVector CurrentPosition = GetActorLocation();
	// Calculate our target position (our destination).
	FVector TargetPosition = StartingPosition + (DirectionVector * TravelDistance);
	// Increment our current position variable.
	CurrentPosition += DirectionVector * Speed * DeltaTime;
	// If we're within the tolerance range of our destination or origin, flip the direction of travel.
	UE_LOG(LogTemp, Warning, TEXT("Current: %s | Target: %s."), *CurrentPosition.ToString(), *TargetPosition.ToString());
	if (FVector::Dist(CurrentPosition, TargetPosition) <= Tolerance)
	{
		CurrentPosition = TargetPosition;
		StartingPosition = CurrentPosition;
		Direction *= -1;
	}
	// Set our location.
	SetActorLocation(CurrentPosition);
}

