// Fill out your copyright notice in the Description page of Project Settings.


#include "SquarePathActor.h"

// Sets default values
ASquarePathActor::ASquarePathActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup root and visible components.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);

	// Find and set static mesh.
	ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Game/StarterContent/Shapes/Shape_Cube.Shape_Cube"));
	if (Mesh.Succeeded()) mVisibleComponent->SetStaticMesh(Mesh.Object);

	// Find and set material.
	ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("/Game/StarterContent/Materials/M_Concrete_Tiles.M_Concrete_Tiles"));
	if (Mat.Succeeded()) mVisibleComponent->SetMaterial(0, Mat.Object);

	// Setup for movement path.
	SquarePathSideLength = 400.f;
	Speed = 100.f;
	Direction = FVector(1, 0, 0);
}

// Called when the game starts or when spawned
void ASquarePathActor::BeginPlay()
{
	Super::BeginPlay();
	
	InitialPosition = GetActorLocation();
}

// Called every frame
void ASquarePathActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Get next position we will be moving to.
	FVector CurrentPosition = GetActorLocation() + (Direction * Speed * DeltaTime);

	if (Direction.X == 1.f && CurrentPosition.X - InitialPosition.X >= SquarePathSideLength) {
		// UE_LOG(LogTemp, Warning, TEXT("Move right."));
		CurrentPosition.X = InitialPosition.X + SquarePathSideLength;
		Direction.X = 0;
		Direction.Y = 1.f;
	}

	if (Direction.Y == 1.f && CurrentPosition.Y - InitialPosition.Y >= SquarePathSideLength) {
		// UE_LOG(LogTemp, Warning, TEXT("Move down."));
		CurrentPosition.Y = InitialPosition.Y + SquarePathSideLength;
		Direction.X = -1.f;
		Direction.Y = 0;
	}

	if (Direction.X == -1.f && CurrentPosition.X - InitialPosition.X <= 0) {
		// UE_LOG(LogTemp, Warning, TEXT("Move left."));
		CurrentPosition.X = InitialPosition.X;
		Direction.X = 0;
		Direction.Y = -1.f;
	}

	if (Direction.Y == -1.f && CurrentPosition.Y - InitialPosition.Y <= 0) {
		// UE_LOG(LogTemp, Warning, TEXT("Move up."));
		CurrentPosition.Y = InitialPosition.Y;
		Direction.X = 1.f;
		Direction.Y = 0;
	}

	SetActorLocation(CurrentPosition);
}

