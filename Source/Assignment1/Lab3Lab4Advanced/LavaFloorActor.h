// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetSystemLibrary.h"
#include "LavaFloorActor.generated.h"

UCLASS()
class ASSIGNMENT1_API ALavaFloorActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALavaFloorActor();

	// Visible component for the actor.
	UPROPERTY(EditAnywhere);
	UStaticMeshComponent *mVisibleComponent;

	// Targets that are killed on contact.
	UPROPERTY(EditAnywhere)
		TArray<AActor*> Killable;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
