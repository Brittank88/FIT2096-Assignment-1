// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterPawn.h"
#include "ShootableActor.h"
#include "Components/LineBatchComponent.h"

// Sets default values
ACharacterPawn::ACharacterPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// This actor receives input from the player.
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// Setup root and visible components, and attach the camera.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	mCamera->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ACharacterPawn::BeginPlay()
{
	Super::BeginPlay();
}

void ACharacterPawn::MoveForward(float axisValue)
{
	mMovementInput.X = axisValue;
}

void ACharacterPawn::MoveRight(float axisValue)
{
	mMovementInput.Y = axisValue;
}

void ACharacterPawn::Pitch(float axisValue)
{
	mRotationInput.Pitch = axisValue;
}

void ACharacterPawn::Yaw(float axisValue)
{
	mRotationInput.Yaw = axisValue;
}

void ACharacterPawn::Shoot()
{
	// Print a message to the output log
	UE_LOG(LogTemp, Warning, TEXT("Player: Bang!"));
	// Set up a trace to see if we can hit something
	FHitResult linetraceResult;
	// Stat position of check
	FVector startTrace = GetActorLocation();
	// End position of check
	FVector endTrace = (GetActorForwardVector() * 7500.0f) + startTrace;
	FCollisionQueryParams params;
	// Draw our trace.
	GetWorld()->PersistentLineBatcher->DrawLine(startTrace - FVector(0.f, 0.f, 10.f), endTrace, FLinearColor::Red, 1.f, 2.f, .1f);
	// Attempt to check
	bool isHit = GetWorld()->LineTraceSingleByChannel(linetraceResult, startTrace, endTrace, ECC_WorldStatic, params);
	// If something was hit
	if (isHit) {
		// Attempt to cast to our shootable object
		AShootableActor *shootTarget = Cast<AShootableActor>(linetraceResult.GetActor());
		if (shootTarget) {
			// If successful case then display hit message and call OnBulletHit
			UE_LOG(LogTemp, Warning, TEXT("Hit Shoot Target"));
			shootTarget->OnBulletHit();
		}
	}
}

// Called every frame
void ACharacterPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!mMovementInput.IsZero()) {
		// We will come back to what this means in the next few weeks
		mMovementInput.Normalize();

		FRotator movementRotator = GetActorForwardVector().ToOrientationRotator();
		movementRotator.Pitch = 0;

		SetActorLocation(GetActorLocation() + (movementRotator.RotateVector(mMovementInput) * speed * DeltaTime));
	}

	if (!mRotationInput.IsZero()) {
		FRotator newRotation = GetActorRotation().Add(mRotationInput.Pitch, mRotationInput.Yaw, mRotationInput.Roll);
		newRotation.Pitch = FMath::Clamp(newRotation.Pitch, -89.f, 89.f);

		SetActorRotation(newRotation);
	}
}

// Called to bind functionality to input
void ACharacterPawn::SetupPlayerInputComponent(UInputComponent *PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveX", this, &ACharacterPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveY", this, &ACharacterPawn::MoveRight);
	PlayerInputComponent->BindAxis("Pitch", this, &ACharacterPawn::Pitch);
	PlayerInputComponent->BindAxis("Yaw", this, &ACharacterPawn::Yaw);
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &ACharacterPawn::Shoot);
}
