// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Pawn.h"
#include "CollisionQueryParams.h"

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "CharacterPawn.generated.h"

UCLASS()
class ASSIGNMENT1_API ACharacterPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACharacterPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Player view object.
	UCameraComponent *mCamera;

	// Player control methods.
	virtual void MoveForward(float axisValue);
	virtual void MoveRight(float axisValue);
	virtual void Pitch(float axisValue);
	virtual void Yaw(float axisValue);
	virtual void Shoot();

	// Movement speed.
	float speed = 500.0f;

	// Translation and rotation objects.
	FVector mMovementInput;
	FRotator mRotationInput;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

};
