// Fill out your copyright notice in the Description page of Project Settings.

#include "ShootableActor.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AShootableActor::AShootableActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Setup root and visible components.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	mVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	mVisibleComponent->SetupAttachment(RootComponent);

	// Find and set static mesh.
	ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("/Game/StarterContent/Shapes/Shape_NarrowCapsule.Shape_NarrowCapsule"));
	if (Mesh.Succeeded()) mVisibleComponent->SetStaticMesh(Mesh.Object);

	// Find and set material.
	ConstructorHelpers::FObjectFinder<UMaterial> Mat(TEXT("/Game/StarterContent/Materials/M_Tech_Hex_Tile.M_Tech_Hex_Tile"));
	if (Mat.Succeeded()) mVisibleComponent->SetMaterial(0, Mat.Object);

	// Find explosion assets.
	ExplosionPS = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'")).Object;
	ExplosionSC = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundBase'/Game/StarterContent/Audio/Explosion_Cue.Explosion_Cue'")).Object;

	// Setup for dodging.
	Speed = 50.f;
	bIsDodgingLeft = false;
	bIsDodgingLeft = false;

	// Setup for jumping.
	JumpHeight = 50.f;
	JumpDistance = 0.f;
	JumpDirection = 1.f;
	bIsJumping = false;
}

void AShootableActor::OnBulletHit()
{
	// Play explosion effects.
	if (ExplosionPS != NULL) UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionPS, mVisibleComponent->GetComponentTransform());
	if (ExplosionSC != NULL) UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSC, mVisibleComponent->GetComponentLocation());

	// Clear our timers for performance.
	GetWorldTimerManager().ClearTimer(DodgeTimerHandle);
	GetWorldTimerManager().ClearTimer(JumpTimerHandle);

	// Destroy this actor.
	Destroy();
}

// Called when the game starts or when spawned
void AShootableActor::BeginPlay()
{
	Super::BeginPlay();

	// Setup dodge timer.
	GetWorldTimerManager().SetTimer(DodgeTimerHandle, [this]() {
		UE_LOG(LogTemp, Warning, TEXT("Actor: Dodge!"));

		// Randomly decide a dodging action.
		int DodgeAction = FMath::RandRange(0, 2);
		if (DodgeAction == 0) {
			bIsDodgingRight = true;
			bIsDodgingLeft = false;
		} else if (DodgeAction == 1) {
			bIsDodgingRight = false;
			bIsDodgingLeft = true;
		} else {
			bIsDodgingRight = false;
			bIsDodgingLeft = false;
		}
	}, 2.f, true);

	// Setup jump timer.
	GetWorldTimerManager().SetTimer(JumpTimerHandle, [this]() {
		bIsJumping = FMath::RandBool();
	}, 3.f, true);
}

// Called every frame
void AShootableActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Dodge in direction decided by boolean values.
	FVector DodgeDirection = GetActorRightVector() * FVector(0, bIsDodgingRight ? 1 : (bIsDodgingLeft ? -1 : 0), 0);
	SetActorLocation(GetActorLocation() + DodgeDirection * Speed * DeltaTime);

	// Jump code.
	if (bIsJumping) {
		// Get the jump direction vector.
		FVector JumpDirectionVec = GetActorUpVector() * FVector(0, 0, JumpDirection);
		// Get the offset that we will be applying this tick, in the direction of the jump.
		FVector LocationOffset = JumpDirectionVec * Speed * DeltaTime;

		// Tracking how high we've jumped so far.
		JumpDistance += LocationOffset.Z;
		// UE_LOG(LogTemp, Warning, TEXT("JumpDistance: %f"), JumpDistance);
		// Change the jump direction if we have reached our maximum jump height.
		if (JumpDistance >= JumpHeight) JumpDirection *= -1;
		if (JumpDistance <= 0) {
			// This corrects the final height if it was going to place the actor further down than it began.
			LocationOffset.Z += -JumpDistance;
			// Reset the jump direction.
			JumpDirection *= -1;
			// We are no longer jumping.
			bIsJumping = false;
		}

		// Set the actor location to our new value.
		SetActorLocation(GetActorLocation() + LocationOffset);
	}
}

