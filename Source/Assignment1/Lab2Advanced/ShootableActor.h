// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShootableActor.generated.h"

UCLASS()
class ASSIGNMENT1_API AShootableActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShootableActor();

	// Handler for when the actor is shot by the player.
	virtual void OnBulletHit();

	// Dodging speed.
	UPROPERTY(EditAnywhere)
		float Speed;

	// Jumping height.
	UPROPERTY(EditAnywhere)
		float JumpHeight;

	// Visible component for the actor.
	UPROPERTY(EditAnywhere);
	UStaticMeshComponent *mVisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Track whether we are dodging or not.
	UPROPERTY(VisibleAnywhere)
		bool bIsDodgingLeft;
	UPROPERTY(VisibleAnywhere)
		bool bIsDodgingRight;

	// Dodge timer.
	UPROPERTY(VisibleAnywhere)
		FTimerHandle DodgeTimerHandle;

	// Track whether we are jumping or not.
	UPROPERTY(VisibleAnywhere)
		bool bIsJumping;

	// Track how much height has been added by our current jump.
	UPROPERTY(VisibleAnywhere)
		float JumpDistance;

	// Track how much height has been added by our current jump.
	UPROPERTY(VisibleAnywhere)
		float JumpDirection;

	// Jump timer.
	UPROPERTY(VisibleAnywhere)
		FTimerHandle JumpTimerHandle;

	// Explosion effects on being shot.
	UPROPERTY(VisibleAnywhere)
		class UParticleSystem *ExplosionPS;
	UPROPERTY(VisibleAnywhere)
		class USoundBase *ExplosionSC;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
