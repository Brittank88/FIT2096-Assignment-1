ALL MAPS TO REVIEW ARE IN THE DIRECTORIES IN THE ASSIGNMENTFILES FOLDER IN THE CONTENT BROWSER.

Part 1:
- Make the shootable actor move randomly left and right - complete!
- Make the shootable actor jump every few seconds - complete!
	
Part 2:
- The floor of the level is lava. If the player touches it the game is over, and the game should
quit. You should use vector math to determine if the player is on the floor. - complete!
- Reaching an end platform will end the game as the player has won - incomplete :(
- At least one platform should move around in a square path that continues indefinitely - complete!
- At least one platform should move in a zig zag path back and forth - complete!
- At least one platform should begins moving in a back and forth movement for 5 seconds - complete!
once the player has stood Infront of a scanner block - complete!
- At least three spheres should chase the player once they have stood in front of the above
scanner block. Touching a sphere is game over. - complete!

Part 3:
Update the spheres that chase the player to a cone that is rotated to always face the player
- At least four platforms should be orbiting a stationary platform - complete!
- At least four platforms should be orbiting a platform that moves side to side. This will
require the platforms to move with it. - complete!
- At least one platform that rotates around that does not rotate the player when the player is
standing on it - incomplete :(
- At least one platform that does a half orbit back and forth around an origin point - incomplete :(